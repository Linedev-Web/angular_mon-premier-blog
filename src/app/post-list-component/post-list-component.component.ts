import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-post-list-component',
  templateUrl: './post-list-component.component.html',
  styleUrls: ['./post-list-component.component.scss']
})
export class PostListComponentComponent implements OnInit {

  @Input() blogTitle: string;
  @Input() blogContent: string;
  @Input() blogLoveIts: number;
  @Input() blogData: Date;

  constructor() {
  }

  ngOnInit() {
  }

  addLoveIts() {
    this.blogLoveIts++;
  }

  removeLoeIts() {
    this.blogLoveIts--;
  }


}
