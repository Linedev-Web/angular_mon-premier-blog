import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'mon-premier-blog';

  posts = [
    {
      title: 'Mon premier post',
      content: 'Appropriately drive professional schemas before resource-leveling services. Professionally cultivate dynamic supply chains vis-a-vis installed base e-tailers.',
      loveIts: 0,
      createDate: new Date(),
    },
    {
      title: 'Mon deuxième post',
      content: 'Appropriately drive professional schemas before resource-leveling services. Professionally cultivate dynamic supply chains vis-a-vis installed base e-tailers.',
      loveIts: 0,
      createDate: new Date(),
    },
    {
      title: 'Encore un post',
      content: 'Appropriately drive professional schemas before resource-leveling services. Professionally cultivate dynamic supply chains vis-a-vis installed base e-tailers.',
      loveIts: 0,
      createDate: new Date(),
    },
  ];


}
